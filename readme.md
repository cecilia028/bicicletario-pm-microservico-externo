# Microservi�o de Externo

Microservi�o respons�vel pelas opera��es externas ao modelo. Esse microservi�o possui duas rotas `HTTP`, sendo elas:

## Get cobranca (/cobranca):
Opera��o `GET` que recupera uma cobrança baseada no id.

### C�digo HTTP:

Esse m�todo possui dois c�digos de retorno poss�veis, sendo eles:

	1. 200: search results match criteria
	2. 404: not found

## Post cobrança (/cobranca):
Opera��o `POST` que cadastrada uma nova cobrança no sistema. Recebe um objeto do tipo:

```
	{
  "ciclista": "d290f1ee-6c54-4b01-90e6-d701748f0851" (UUID),
  "valor": 200.00 (Integer)
}
```

### C�digo HTTP:

Esse m�todo possui dois c�digos de retorno poss�veis, sendo eles:

	1. 200: item created
	2. 422: Invalid data
