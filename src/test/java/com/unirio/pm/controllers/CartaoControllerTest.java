package com.unirio.pm.controllers;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

import com.unirio.pm.domain.Cartao;

import controllers.CartaoController;
import controllers.EmailController;
import controllers.Server;
import io.javalin.http.Context;
import io.javalin.plugin.json.JavalinJson;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

public class CartaoControllerTest {

	private static Server app = new Server();
    
//    @Test
//    public void deveValidarCartao() {
//    	 String id = "232";
//         String cvv = "359";
//         String nome = "Lucas";
//         String num = "23523553";
//         String validade = "20/11/2020";
//         Cartao cartao = Cartao.builder().id(id).cvv(cvv).nomeTitular(nome).numero(num).validade(validade).build();
//        assertEquals(true, this.cartaoController.validaCartaoCredito(cartao));
//    }
    
    @BeforeAll
    static void init() {
        app.start(7010);
    }
    
    @AfterAll
    static void afterAll(){
        app.stop();
    }
    
    @Test
    public void serverOn() {
        assertNotNull(app );
    }
    
    @Test
    public void validaNaoCartaoCredito() {
         Cartao cartao = null;
        assertNull(cartao);
    }
    
    @Test
    public void postSucessoEnviarEmail() {
    	HttpResponse response = Unirest.post("http://localhost:7010/enviarEmail?email=email0@email.com&mensagem=teste msg").asString();
        assertEquals(200, response.getStatus());
    }
    
    @Test
    public void postFalhaEnviarEmail() {
    	HttpResponse response = Unirest.post("http://localhost:7010/enviarEmail?email=email0l.com&mensagem=teste msg").asString();
        assertEquals(422, response.getStatus());
    }
    
    @Test
    public void postSucessoCobranca() {
    	HttpResponse response = Unirest.post("http://localhost:7010/cobranca?valor=30&ciclista=1").asString();
        assertEquals(200, response.getStatus());
    }
    
    @Test
    public void postFalhaCobranca() {
    	HttpResponse response = Unirest.post("http://localhost:7010/cobranca?").asString();
        assertEquals(422, response.getStatus());
    }
    
    @Test
    public void postFalhaCobrancaCiclista() {
    	app.start(7010);
    	HttpResponse response = Unirest.post("http://localhost:7010/cobranca/aa1").asString();
        assertEquals(422, response.getStatus());
    }
    
    @Test
    public void postSucessoCobrancaCiclista() {
    	HttpResponse response = Unirest.post("http://localhost:7010/cobranca/1").asString();
        assertEquals(200, response.getStatus());
    }
    
    @Test
    public void getFalhaCobrancaCiclista() {
    	HttpResponse response = Unirest.get("http://localhost:7010/cobranca/199").asString();
        assertEquals(404, response.getStatus());
    }
    
    @Test
    public void getSucessoCobrancaCiclista() {
    	HttpResponse response = Unirest.get("http://localhost:7010/cobranca/1").asString();
        assertEquals(200, response.getStatus());
    }
    
    @Test
    public void postFalhaFilaCobrancaCiclista() {
    	HttpResponse response = Unirest.post("http://localhost:7010/filaCobranca?valor=100&ciclista=aa1").asString();
        assertEquals(422, response.getStatus());
    }
    
    @Test
    public void postSucessoFilaCobrancaCiclista() {
    	HttpResponse response = Unirest.post("http://localhost:7010/filaCobranca?ciclista=1&valor=100").asString();
        assertEquals(200, response.getStatus());
    }
    
    @Test
    public void postSucessoValidaCiclista() {
    	HttpResponse response = Unirest.post("http://localhost:7010/validaCartaoDeCredito/1?cvv=847&nomeTitular=lucas&validade=2029-02-02&numero=4564710000000004").asString();
        assertEquals(200, response.getStatus());
    }
    
    @Test
    public void postFalhaValidaCiclista() {
    	HttpResponse response = Unirest.post("http://localhost:7010/validaCartaoDeCredito/1?nomeTitular=lucas&validade=2029-02-02&numero=asdad").asString();
        assertEquals(422, response.getStatus());
    }
    
    @Test
    public void getSucessoCCCiclista() {
    	HttpResponse response = Unirest.get("http://localhost:7010/cartaoDeCredito/1").asString();
        assertEquals(200, response.getStatus());
    }
    
    @Test
    public void getFalhaCCCiclista() {
    	HttpResponse response = Unirest.get("http://localhost:7010/cartaoDeCredito/asdadasd").asString();
        assertEquals(422, response.getStatus());
    }
    
    @Test
    public void putSucessoCCCiclista() {
    	HttpResponse response = Unirest.put("http://localhost:7010/cartaoDeCredito/1?cvv=847&nomeTitular=outroNome&validade=2028-12-02&numero=4564710000000004").asString();
        assertEquals(200, response.getStatus());
    }
    
    @Test
    public void putFalhaCCCiclista() {
    	HttpResponse response = Unirest.put("http://localhost:7010/cartaoDeCredito/1?nomeTitular=lucas&validade=2029-02-02&numero=asdad").asString();
        assertEquals(422, response.getStatus());
    }
    
}
