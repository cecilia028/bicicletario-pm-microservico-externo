package com.unirio.pm.services;

import com.unirio.pm.domain.Cartao;
import com.unirio.pm.domain.Cobranca;
import com.unirio.pm.util.Validator;
import org.junit.Test;
import static org.junit.Assert.assertEquals;


public class DBOServiceTest {

    @Test
    public void deveVerificarEmailExiste() {
        assertEquals(false, DBOService.validaEmail("email"));
    }

    @Test
    public void deveNotificarEmail() {
        assertEquals(true, Validator.notificaEmail("email@email.com"));
    }
    
    @Test
    public void deveVerificarCartaoExiste() {
    	 String id = "232";
         String cvv = "359";
         String nome = "Lucas";
         String num = "23523553";
         String validade = "20/11/2020";
         Cartao cartao = Cartao.builder().id(id).cvv(cvv).nomeTitular(nome).numero(num).validade(validade).build();
        assertEquals(false, DBOService.validaCartaoCredito(cartao));
    }
    

}
