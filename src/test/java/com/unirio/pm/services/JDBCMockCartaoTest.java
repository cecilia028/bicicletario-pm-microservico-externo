package com.unirio.pm.services;

import com.unirio.pm.domain.Cartao;
import com.unirio.pm.domain.Cobranca;
import com.unirio.pm.util.Validator;


import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;


public class JDBCMockCartaoTest {

	JDBCMockCartao mock = new JDBCMockCartao();
	
    @Test
    public void deleteCartao() {
    	assertEquals(true, mock.deleteData("1"));
    }

    @Test
    public void deleteFCartao() {
    	assertEquals(false, mock.deleteData("1asdad"));
    }
    
    @Test
    public void updateCartao() {
    	 String id = "232";
       String cvv = "359";
       String nome = "Lucas";
       String num = "23523553";
       String validade = "20/11/2020";
       Cartao cartao = Cartao.builder().id(id).cvv(cvv).nomeTitular(nome).numero(num).validade(validade).build();
       cartao.setCvv("999");
       mock.updateCartao(cartao);
    	assertEquals("999", cartao.getCvv());
    }
    
    @Test
    public void updateFCartao() {
    	 String id = "232";
         String cvv = "359";
         String nome = "Lucas";
         String num = "23523553";
         String validade = "20/11/2020";
         Cartao cartao = Cartao.builder().id(id).cvv(cvv).nomeTitular(nome).numero(num).validade(validade).build();
         mock.updateCartao(cartao);
      	assertNotEquals("999", cartao.getCvv());
    }
    
    
    
    
    

    

}
