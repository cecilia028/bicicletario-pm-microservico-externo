package com.unirio.pm.domain;

import org.jetbrains.annotations.Nullable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Cartao {
    @JsonProperty("id")
    private String id;
    
    @JsonProperty("idCiclista")
    private String idCiclista;

    @JsonProperty("nomeTitular")
    private String nomeTitular;

    @JsonProperty("numero")
    private String numero;

    @JsonProperty("validade")
    private String validade;

    @JsonProperty("cvv")
    private String cvv;
}
