package com.unirio.pm.services;

import java.util.ArrayList;
import java.util.List;

import com.unirio.pm.domain.Email;
import com.unirio.pm.domain.Email.EmailBuilder;

public class JDBCMockEmail {
	
	/** lista de emails */
	public final List<Email> banco = new ArrayList<>();
	
	/** aloca emails num repositorio **/
	    public JDBCMockEmail() {
	        for (int i = 0; i < 10; i++) {
	        	EmailBuilder email = Email.builder().uuid(String.valueOf(i)).email("email"+String.valueOf(i)+"@email.com");
	            banco.add(email.build());
	        }
	    }
	    /** atualiza os emails ou adiciona eles **/
	    public void updateData(Email mailParam) {
	        if (!this.existeEmail(mailParam.getEmail())) {
	            banco.add(mailParam);
	        }else{
	            for (Email email : banco) {
	                if (email.getEmail().equalsIgnoreCase(mailParam.getEmail())) {
	                    banco.set(banco.indexOf(email), mailParam);
	                }
	            }
	        }
	    }

	    public Boolean deleteData(String id) {
	        for (Email email : banco) {
	            if (email.getUuid() != null && email.getUuid().equals(id)) {
	                banco.remove(email);                
	                return true;
	            }
	        }
	        return false;
	    }

		public boolean existeEmail(String emailParam) {
			for (Email email : banco) {
	            if (email.getEmail() != null && emailParam != null && email.getEmail().equalsIgnoreCase(emailParam)) {
	                return true;
	            }
	        }
	        return false;
		}
}
