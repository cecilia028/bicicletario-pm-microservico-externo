package controllers;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.fasterxml.uuid.Generators;
import com.unirio.pm.domain.Cobranca;
import com.unirio.pm.domain.PagamentoStatus;
import com.unirio.pm.services.JDBCMockCobranca;
import com.unirio.pm.util.Validator;

import io.javalin.http.Context;

public class CobrancaController {
	private static CobrancaController cobrancaController;
	public static final JDBCMockCobranca mock = new JDBCMockCobranca();
	
	public static CobrancaController getCobrancaController() {
		if (cobrancaController == null) {
			cobrancaController = new CobrancaController();
		}
		return cobrancaController;
	}

	private Cobranca geraNovaCobranca(Cobranca cobranca) {
		if (cobranca.getId() == null) {
			cobranca.setId(Generators.timeBasedGenerator().generate().toString());
		}
		if (cobranca.getHoraSolicitacao() == null) {
			cobranca.setHoraSolicitacao(new Date().toString());
		}
		return cobranca;
	}

	public Cobranca realizaCobranca(Context ctx) {
		if(Validator.isNullOrEmpty(ctx.pathParam("idCiclista"))) {
			return null;
		} else if(mock.getCobrancaByIdCiclista(ctx.pathParam("idCiclista") ) == null) {
			return null;
		}else {
			Cobranca cobranca = Cobranca.builder().ciclista(ctx.pathParam("idCiclista")).status(PagamentoStatus.PENDENTE).build();
			cobranca = mock.cobrancaTotalByCiclista(cobranca);
			cobranca.setHoraFinalizacao(new Date().toString());
			
			return cobranca;
		}
//		boolean removido = mock.deleteData(cobranca.getId());
//		cobranca.setHoraFinalizacao(new Date().toString());
//		return removido;
	}
	
	

	public boolean addCobranca(Cobranca c) {
		this.geraNovaCobranca(c);
		c.setStatus(PagamentoStatus.PENDENTE);
		if (mock.getCobrancaByIdCiclista(c.getCiclista()) != null) {
			mock.createNovaCobranca(c);
			return true;
		} else {
			return false;
		}
	}

	/** Quando aluga **/
	public Cobranca criarCobranca(Context ctx) {
		if(Validator.isNullOrEmpty(ctx.queryParam("valor")) || Validator.isNullOrEmpty(ctx.queryParam("ciclista"))) {
			return null;
		} 
		String horaSolicitacao = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime());
		Cobranca cobranca = Cobranca.builder().id(String.valueOf(mock.getAllCobrancas().size())).valor(Integer.parseInt(ctx.queryParam("valor"))).ciclista(ctx.queryParam("ciclista")).status(PagamentoStatus.PENDENTE).horaSolicitacao(horaSolicitacao).build();
		mock.updateData(cobranca);
		return cobranca;
	}
	
	

	public Cobranca getCobranca(String id) {
		return mock.getCobrancaById(id);
	}

	public List<Cobranca> getAllCobrancas() {
		return mock.getAllCobrancas();
	}

	public boolean deleteCobranca(String id) {
		return mock.deleteData(id);
	}

	public Cobranca adicionaNovaCobranca(Context ctx) {
		if(Validator.isNullOrEmpty(ctx.queryParam("valor")) || Validator.isNullOrEmpty(ctx.queryParam("ciclista"))) {
			return null;
		} 
		return Cobranca.builder().valor(Integer.parseInt(ctx.queryParam("valor"))).ciclista(ctx.queryParam("ciclista")).build();
	}
	
	

}
