package controllers;

import com.fasterxml.uuid.Generators;
import com.unirio.pm.domain.Cartao;
import com.unirio.pm.services.CardAPI;
import com.unirio.pm.services.JDBCMockCartao;
import com.unirio.pm.util.Error;
import com.unirio.pm.util.Validator;

import io.javalin.http.Context;

public class CartaoController {
    private static CartaoController cartaoController;
    private static final String IDCICLISTA = "idCiclista";
    
    public static final JDBCMockCartao mock = new JDBCMockCartao();

    public static CartaoController getCartaoController() {
        if (cartaoController == null) {
            cartaoController = new CartaoController();
        }
        return cartaoController;
    }

    public boolean validaCartaoCredito(Cartao cartao) {
    	return Validator.validaCartao(cartao);
    }
    
    public Cartao novoCartaoCredito(Context ctx) { 
    	if(!Validator.isNullOrEmpty(ctx.pathParam(IDCICLISTA)) && !Validator.isNullOrEmpty(ctx.queryParam("nomeTitular")) && 
    	  !Validator.isNullOrEmpty(ctx.queryParam("numero")) && !Validator.isNullOrEmpty(ctx.queryParam("validade")) && 
    	  !Validator.isNullOrEmpty(ctx.queryParam("cvv"))) {
			Cartao cartao = Cartao.builder().id(Generators.timeBasedGenerator().generate().toString()).nomeTitular(ctx.queryParam("nomeTitular")).numero(ctx.queryParam("numero"))
					.validade(ctx.queryParam("validade")).cvv(ctx.queryParam("cvv")).idCiclista(ctx.pathParam(IDCICLISTA)).build();
	    	boolean valido = validaCartaoCredito(cartao);
	    	if (valido) {
	    		CardAPI.cadastrarCartao(cartao);
	    		mock.updateData(cartao);
	    		return cartao;	
			}
    	}
        return null;
    }

	public static void getCartaoCreditoByIdCiclista(Context ctx) {
		if(!Validator.isNullOrEmpty(ctx.pathParam(IDCICLISTA))) {
			Cartao cartao = mock.getCartaoByIdCiclista(ctx.pathParam(IDCICLISTA));
			if(cartao != null) {
				ctx.status(200).json(cartao);
			}else {
				ctx.status(422).json(Error.builder().codigo("422").mensagem("Dados Inválidos").build());
				}
		}else {
		    ctx.status(404).json(Error.builder().codigo("404").mensagem("Não encontrado").build());
		}
	}

	public static void putCartaoCreditoByIdCiclista(Context ctx) {
		Cartao cartao = mock.getCartaoByIdCiclista(ctx.pathParam(IDCICLISTA));
		if(!Validator.isNullOrEmpty(cartao.getIdCiclista())) {
			cartao = updateCartao(ctx, cartao);
			if(Validator.validaCartao(cartao)) {
				mock.updateCartaoByIdCiclista(cartao);
				ctx.status(200).json(Error.builder().codigo("200").mensagem("Dados Atualizados").build());
			}else {
				ctx.status(422).json(Error.builder().codigo("422").mensagem("Dados Inválidos").build());
				}
		}else {
		    ctx.status(404).json(Error.builder().codigo("404").mensagem("Não encontrado").build());
		}
	}

	private static Cartao updateCartao(Context ctx, Cartao cartao) {
		if(!Validator.isNullOrEmpty(ctx.queryParam("cvv"))) {
			  cartao.setCvv(ctx.queryParam("cvv"));
		}
		if(!Validator.isNullOrEmpty(ctx.queryParam("nomeTitular"))) {
			  cartao.setNomeTitular(ctx.queryParam("nomeTitular"));
		}
		if(!Validator.isNullOrEmpty(ctx.queryParam("validade"))) {
			  cartao.setValidade(ctx.queryParam("validade"));
		}
		if(!Validator.isNullOrEmpty(ctx.queryParam("numero"))) {
			  cartao.setNumero(ctx.queryParam("numero"));
		}
		return cartao;
	}

}
